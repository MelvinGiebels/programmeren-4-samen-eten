const express = require("express");
const router = express.Router();
const participateController = require("../controllers/participateController.js");
const authenticationController = require("../controllers/authenticationController");

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
  console.log("Time: ", Date.now());
  next();
});

// UC-401 Aanmelden voor maaltijd
router.post(
  "/studenthome/:homeId/meal/:mealId/signup",
  authenticationController.validateToken,
  participateController.signUpMeal
);

// UC-402 Afmelden voor maaltijd
router.delete(
  "/studenthome/:homeId/meal/:mealId/signoff",
  authenticationController.validateToken,
  participateController.signOffMeal
);

// UC-403 Lijst van maaltijden opvragen
router.get(
  "/meal/:mealId/participants",
  authenticationController.validateToken,
  participateController.getParticipants
);

// UC-404 Details van deelnemer opvragen
router.get(
  "/meal/:mealId/participants/:participantId",
  authenticationController.validateToken,
  participateController.getParticipant
);

module.exports = router;
