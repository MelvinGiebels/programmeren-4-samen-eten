const express = require("express");
const router = express.Router();
const studentHomeController = require("../controllers/studentHomeController");
const authenticationController = require("../controllers/authenticationController");

// UC-201 Maak studentenhuis
router.post(
  "",
  authenticationController.validateToken,
  studentHomeController.validateStudentHome,
  studentHomeController.createStudentHome
);

// UC-202 Overzicht van studentenhuizen
router.get("", studentHomeController.getStudentHome);

// UC-203 Get student home using home Id
router.get("/:homeId", studentHomeController.getStudentHomeById);

// UC-204 Studentenhuis wijzigen
router.put(
  "/:homeId",
  authenticationController.validateToken,
  studentHomeController.validateStudentHome,
  studentHomeController.updateStudentHome
);

// UC-205 Studentenhuis verwijderen
router.delete(
  "/:homeId",
  authenticationController.validateToken,
  studentHomeController.deleteStudentHome
);

// UC-206 Gebruiker toevoegen aan studenthuis
router.post(
  "/:homeId/user",
  authenticationController.validateToken,
  studentHomeController.addUserToStudentHome
);

module.exports = router;
