const express = require("express");
const router = express.Router();
const mealsController = require("../controllers/mealsController.js");
const authenticationController = require("../controllers/authenticationController");

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
  console.log("Time: ", Date.now());
  next();
});

// UC-301 Maaltijd aanmaken
router.post(
  "/:homeId/meal",
  authenticationController.validateToken,
  mealsController.validateMeal,
  mealsController.createMeal
);

// UC-302 Maaltijd wijzigen
router.put(
  "/:homeId/meal/:mealId",
  authenticationController.validateToken,
  mealsController.validateMeal,
  mealsController.updateMeal
);

// UC-303 Lijst van maaltijden opvragen
router.get("/:homeId/meal", mealsController.getMeals);

// UC-304 Details van een maaltijd opvragen
router.get("/:homeId/meal/:mealId", mealsController.getMealDetails);

// UC-305 Maaltijd verwijderen
router.delete(
  "/:homeId/meal/:mealId",
  authenticationController.validateToken,
  mealsController.deleteMeal
);

module.exports = router;
