const assert = require("assert");
const logger = require("tracer").colorConsole();
const database = require("../dao/studentHomeDAO");

module.exports = {
  validateStudentHome(req, res, next) {
    logger.debug("Called validateStudentHome");

    try {
      const { name, address, houseNr, postalCode, city, phoneNr } = req.body;

      // Check if value is a valid type
      assert(typeof name === "string", "name is invalid or missing");
      assert(typeof address === "string", "address is invalid or missing");
      assert(typeof houseNr === "number", "houseNr is invalid or missing");
      assert(
        typeof postalCode === "string",
        "postalCode is invalid or missing"
      );
      assert(typeof phoneNr === "string", "phoneNr is invalid or missing");
      assert(typeof city === "string", "city is invalid or missing");

      // Check if postalCode is valid dutch format: 0000AA
      const rege = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i;
      assert(rege.test(postalCode), "postalCode was invalid");

      // Check phoneNr valid formats:
      // (123) 456-7890 | (123)456-7890 | 123-456-7890 | 123.456.7890 | 1234567890 | +31636363634 | 075-63546725
      const re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
      assert(re.test(phoneNr), "phoneNr was invalid");

      logger.debug("StudentHome is valid");
      next();
    } catch (err) {
      logger.error("StudentHome data is Invalid: ", err.message);
      next({ message: err.message, errCode: 400 });
    }
  },

  // UC-201 Maak studentenhuis
  createStudentHome(req, res, next) {
    logger.debug("Called createStudentHome");
    const studentHome = req.body;
    const userId = req.userId; // userId uit JWT token

    database.insertStudentHome(studentHome, userId, (err, result) => {
      if (err) {
        next(err);
      }
      if (result) {
        logger.debug("StudentHome added to database");
        res.status(200).json({
          status: "success",
          result: {
            id: result.insertId,
            ...studentHome,
          },
        });
      }
    });
  },

  // UC-202 Overzicht van studentenhuizen
  getStudentHome(req, res, next) {
    logger.debug("Called getStudentHome");
    const name = req.query.name;
    const city = req.query.city;

    database.selectStudentHome(name, city, (err, result) => {
      if (err) {
        next(err);
      }
      if (result) {
        logger.debug("GetStudentHome was successfull");
        res.status(200).json({ status: "success", result: result });
      }
    });
  },

  //UC-203 Details van studentenhuis
  getStudentHomeById(req, res, next) {
    logger.debug("Called getStudentHomeById");
    const homeId = req.params.homeId;

    database.getStudentHomeById(homeId, (err, result) => {
      if (err) {
        next(err);
      }
      if (result) {
        logger.debug("GetStudentHome by id was successfull");
        res.status(200).json({ status: "success", result: result });
      }
    });
  },

  // UC-204 Studentenhuis wijzigen
  updateStudentHome(req, res, next) {
    logger.debug("Called update StudentHome");
    const homeId = req.params.homeId;
    const studentHome = req.body;
    const userId = req.userId; // userId uit JWT token

    database.getStudentHomeById(homeId, (err, result) => {
      if (err) {
        next(err);
      }
      if (result) {
        database.validateUser(homeId, userId, (error, result) => {
          if (error) {
            next(error);
          }
          if (result) {
            database.updateStudentHome(
              homeId,
              studentHome,
              userId,
              (err, result) => {
                if (err) {
                  next(err);
                }
                if (result) {
                  logger.debug("Update student home by id was successfull");
                  res.status(200).json({
                    status: "success",
                    result: {
                      id: Number(homeId),
                      ...studentHome,
                    },
                  });
                }
              }
            );
          }
        });
      }
    });
  },

  // UC-205 Studentenhuis verwijderen
  deleteStudentHome(req, res, next) {
    logger.debug("Called delete studentHome");

    const homeId = req.params.homeId;
    const userId = req.userId; // userId uit JWT token

    database.getStudentHomeById(homeId, (err, result) => {
      if (err) {
        next(err);
      }
      if (result) {
        database.validateUser(homeId, userId, (error, result) => {
          if (error) {
            next(error);
          }
          if (result) {
            database.deleteStudentHome(
              homeId,
              userId,
              (error, resultDelete) => {
                if (error) {
                  next(error);
                }
                if (resultDelete) {
                  logger.debug("Delete student home was successfull");
                  res.status(200).json({ status: "success", result: result });
                }
              }
            );
          }
        });
      }
    });
  },

  // UC-206 Gebruiker toevoegen aan studentenhuis
  addUserToStudentHome(req, res, next) {
    const homeId = req.params.homeId;
    const userAddId = req.body.id;
    const userId = req.userId; // userId uit JWT token

    if (!userAddId) {
      res.status(400).json({
        error: "Some error occurred",
        message: "Missing id in request body",
      });
    } else {
      database.validateUser(homeId, userId, (error, result) => {
        if (error) {
          next(error);
        }
        if (result) {
          database.insertUserToHomeAdministrators(
            homeId,
            userAddId,
            (error, result) => {
              if (error) {
                next(error);
              }
              if (result) {
                logger.debug("add user to home admins was successfull");
                res.status(200).json({
                  status: "success",
                  result: "User with id: " + userAddId + " has been added",
                });
              }
            }
          );
        }
      });
    }
  },
};
