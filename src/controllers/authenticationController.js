const assert = require("assert");
const jwt = require("jsonwebtoken");
const pool = require("../dao/database");
const logger = require("../config/config").logger;
const jwtSecretKey = require("../config/config").jwtSecretKey;
const bcrypt = require("bcrypt");

module.exports = {
  // UC-101 Registeren
  register(req, res, next) {
    logger.info("register");
    logger.info(req.body);

    /**
     * Query the database to see if the email of the user to be registered already exists.
     */
    pool.getConnection((err, connection) => {
      if (err) {
        logger.error("Error getting connection from pool: " + err.toString());
        res
          .status(500)
          .json({ message: ex.toString(), datetime: new Date().toISOString() });
      }
      if (connection) {
        let { firstname, lastname, email, studentnr, password } = req.body;

        logger.debug(password);

        bcrypt.hash(password, 10, (error, hashPassword) => {
          // Store hash in your password DB.
          if (error) {
            res.status(400).json({
              message: "Hash was not succesfull " + error.message,
              datetime: new Date().toISOString(),
            });
          } else {
            connection.query(
              "INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password`) VALUES (?, ?, ?, ?, ?)",
              [firstname, lastname, email, studentnr, hashPassword],
              (err, rows, fields) => {
                connection.release();
                if (err) {
                  // When the INSERT fails, we assume the user already exists
                  res.status(400).json({
                    message: "This email has already been taken.",
                    datetime: new Date().toISOString(),
                  });
                } else {
                  logger.trace(rows);
                  // Create an object containing the data we want in the payload.
                  // This time we add the id of the newly inserted user
                  const payload = {
                    id: rows.insertId,
                  };
                  // Userinfo returned to the caller.
                  const userinfo = {
                    username: firstname + " " + lastname,
                    token: jwt.sign(payload, jwtSecretKey, { expiresIn: "2h" }),
                  };
                  logger.debug("Registered", userinfo);
                  res.status(200).json(userinfo);
                }
              }
            );
          }
        });
      }
    });
  },

  validateRegister(req, res, next) {
    // Verify that we receive the expected input
    try {
      assert(
        typeof req.body.firstname === "string",
        "firstname is invalid or missing."
      );
      assert(
        typeof req.body.lastname === "string",
        "lastname is invalid or missing."
      );
      assert(
        typeof req.body.email === "string",
        "email is invalid or missing."
      );
      assert(
        typeof req.body.password === "string",
        "password is invalid or missing."
      );

      // Check if email is valid format
      const reg =
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      assert(reg.test(req.body.email), "email was invalid format.");

      next();
    } catch (ex) {
      logger.debug("validateRegister error: ", ex.toString());
      res
        .status(400)
        .json({ message: ex.toString(), datetime: new Date().toISOString() });
    }
  },

  // UC-102 Login
  login(req, res, next) {
    pool.getConnection((err, connection) => {
      if (err) {
        logger.error("Error getting connection from pool");
        res.status(500).json({
          message: err.toString(),
          datetime: new Date().toISOString(),
        });
      }
      if (connection) {
        // 1. Kijk of deze useraccount bestaat.
        connection.query(
          "SELECT `ID`, `Email`, `Password`, `First_Name`, `Last_Name` FROM `user` WHERE `Email` = ?",
          [req.body.email],
          (err, rows, fields) => {
            connection.release();
            if (err) {
              logger.error("Error: ", err.toString());
              res.status(500).json({
                message: err.toString(),
                datetime: new Date().toISOString(),
              });
            } else {
              // 2. Er was een resultaat, check het password.
              logger.info("Result from database: ");
              logger.info(rows);
              if (rows.length) {
                bcrypt
                  .compare(req.body.password, rows[0].Password)
                  .then((match) => {
                    if (rows && rows.length === 1 && match) {
                      logger.info("passwords DID match, sending valid token");
                      // Create an object containing the data we want in the payload.
                      const payload = {
                        id: rows[0].ID,
                      };
                      // Userinfo returned to the caller.
                      const userinfo = {
                        username: rows[0].First_Name + " " + rows[0].Last_Name,
                        token: jwt.sign(payload, jwtSecretKey, {
                          expiresIn: "2h",
                        }),
                      };
                      logger.debug("Logged in, sending: ", userinfo);
                      res.status(200).json(userinfo);
                    } else {
                      logger.info("Password is invalid");
                      res.status(400).json({
                        message: "Password is invalid",
                        datetime: new Date().toISOString(),
                      });
                    }
                  });
              } else {
                logger.info("No User found for email: " + req.body.email);
                res.status(400).json({
                  message: "No User found for email: " + req.body.email,
                  datetime: new Date().toISOString(),
                });
              }
            }
          }
        );
      }
    });
  },

  validateLogin(req, res, next) {
    // Verify that we receive the expected input
    try {
      assert(
        typeof req.body.email === "string",
        "email is invalid or missing."
      );
      assert(
        typeof req.body.password === "string",
        "password is invalid or missing."
      );
      // Check if email is valid format
      const reg =
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      assert(reg.test(req.body.email), "email was invalid format.");

      next();
    } catch (ex) {
      res
        .status(400)
        .json({ message: ex.toString(), datetime: new Date().toISOString() });
    }
  },

  validateToken(req, res, next) {
    logger.info("validateToken called");
    // logger.trace(req.headers)
    // The headers should contain the authorization-field with value 'Bearer [token]'
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      logger.warn("Authorization header missing!");
      res.status(401).json({
        message: "Authorization header missing!",
        datetime: new Date().toISOString(),
      });
    } else {
      // Strip the word 'Bearer ' from the headervalue
      const token = authHeader.substring(7, authHeader.length);

      jwt.verify(token, jwtSecretKey, (err, payload) => {
        if (err) {
          logger.warn("Not authorized");
          res.status(401).json({
            message: "Not authorized",
            datetime: new Date().toISOString(),
          });
        }
        if (payload) {
          logger.debug("token is valid", payload);
          // User heeft toegang. Voeg UserId uit payload toe aan
          // request, voor ieder volgend endpoint.
          req.userId = payload.id;
          next();
        }
      });
    }
  },

  // UC-103 Systeeminfo opvragen
  getInfo(req, res) {
    logger.info("Info endpoint called");
    let result = {
      studentName: "Melvin Giebels",
      studentNr: "2173543",
      description: "Informatica student",
      sonarqubeURL:
        "https://sonarqube.avans-informatica-breda.nl/dashboard?id=samen-eten-programmeren-4-melvin",
    };

    res.status(200).send(result);
  },
};
