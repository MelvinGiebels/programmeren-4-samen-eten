const assert = require("assert");
const logger = require("tracer").colorConsole();
const database = require("../dao/mealsDAO");

module.exports = {
  validateMeal(req, res, next) {
    logger.debug("Called validateMeal");
    try {
      const { name, description, ingredients, allergyInfo, offerDate, price } =
        req.body;

      // Check if date is valid
      const date = new Date(offerDate);
      let validDate = false;
      if (date instanceof Date && !isNaN(date)) {
        validDate = true;
      }

      assert(validDate, "offerDate is invalid or missing");

      // Check if value is a valid type
      assert(typeof name === "string", "name is invalid or missing");
      assert(
        typeof description === "string",
        "description is invalid or missing"
      );
      assert(Array.isArray(ingredients), "ingredients is invalid or missing");
      assert(ingredients.length, "ingredients can't be empty");
      assert(
        typeof allergyInfo === "string",
        "allergyInfo is invalid or missing"
      );
      assert(typeof price === "string", "price is invalid or missing");

      // Check if array contains valid values
      ingredients.forEach((ingredient) => {
        assert(
          typeof ingredient === "string",
          "ingredient: " + ingredient + " must be a string"
        );
      });

      // Check if price is valid format ex: 0.00
      const reg = /^[0-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
      assert(reg.test(price), "price was invalid");

      logger.debug("Meal is valid");
      next();
    } catch (err) {
      logger.error("Meal data is Invalid: ", err.message);
      next({ message: err.message, errCode: 400 });
    }
  },

  // UC-301 Maaltijd aanmaken
  createMeal(req, res, next) {
    logger.debug("Called postMeal");
    const studentHomeId = req.params.homeId;
    const userId = req.userId; // userId uit JWT token
    const meal = req.body;

    database.insertMeal(studentHomeId, userId, meal, (err, result) => {
      if (err) {
        next(err);
      }
      if (result) {
        logger.debug("Meal added to database");
        res.status(200).json({
          status: "success",
          result: {
            id: result.insertId,
            ...meal,
          },
        });
      }
    });
  },

  // UC-302 Maaltijd wijzigen
  updateMeal(req, res, next) {
    logger.debug("Called updateMeal");
    const studentHomeId = req.params.homeId;
    const userId = req.userId; // userId uit JWT token
    const mealId = req.params.mealId;
    const meal = req.body;

    database.getStudentHomeMeals(studentHomeId, (err, result) => {
      if (err) {
        next(err);
      }
      if (result) {
        let mealValidate = result.filter(function (e) {
          return e.ID == mealId;
        });
        if (!mealValidate.length) {
          const error = {
            message: "getMealsDetails: No meal found for id: " + mealId,
            errCode: 404,
          };
          next(error);
        } else {
          database.updateMeal(
            studentHomeId,
            userId,
            mealId,
            meal,
            (err, result) => {
              if (err) {
                next(err);
              }

              if (result) {
                logger.debug("updateMeal was succesfull");
                res.status(200).json({
                  status: "success",
                  result: {
                    id: Number(mealId),
                    ...meal,
                  },
                });
              }
            }
          );
        }
      }
    });
  },

  // UC-303 Lijst van maaltijden opvragen
  getMeals(req, res, next) {
    logger.debug("Called getMeals");
    const studentHomeId = req.params.homeId;

    database.getStudentHomeMeals(studentHomeId, (err, result) => {
      if (err) {
        next(err);
      }
      if (result) {
        logger.debug(result);
        logger.debug("Get meals was succesfull");
        res.status(200).json({ status: "success", result: result });
      }
    });
  },

  // UC-304 Details van een maaltijd opvragen
  getMealDetails(req, res, next) {
    logger.debug("Called getMealDetails");
    const studentHomeId = req.params.homeId;
    const mealId = Number(req.params.mealId);

    database.getStudentHomeMeals(studentHomeId, (err, result) => {
      if (err) {
        next(err);
      }
      if (result) {
        let meal = result.filter(function (e) {
          return e.ID === mealId;
        });

        if (!meal.length) {
          const error = {
            message: "getMealsDetails: No meal found for id: " + mealId,
            errCode: 404,
          };
          next(error);
        } else {
          logger.debug("Get meals was succesfull");
          res.status(200).json({ status: "success", result: meal });
        }
      }
    });
  },

  // UC-305 Maaltijd verwijderen
  deleteMeal(req, res, next) {
    logger.debug("Called deleteMeal");
    const studentHomeId = req.params.homeId;
    const userId = req.userId; // userId uit JWT token
    const mealId = req.params.mealId;

    database.getStudentHomeMeals(studentHomeId, (err, result) => {
      if (err) {
        next(err);
      }
      if (result) {
        let mealValidate = result.filter(function (e) {
          return e.ID == mealId;
        });
        if (!mealValidate.length) {
          const error = {
            message: "getMealsDetails: No meal found for id: " + mealId,
            errCode: 404,
          };
          next(error);
        } else {
          database.deleteMeal(studentHomeId, userId, mealId, (err, result) => {
            if (err) {
              next(err);
            }
            if (result) {
              logger.debug("Delete meal was successfull");
              res
                .status(200)
                .json({ status: "success", result: result.message });
            }
          });
        }
      }
    });
  },
};
