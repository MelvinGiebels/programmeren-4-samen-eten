const assert = require("assert");
const logger = require("tracer").colorConsole();
const database = require("../dao/participateDAO");
const databaseMeal = require("../dao/mealsDAO");

module.exports = {
  // UC-401 Aanmelden voor maaltijd
  signUpMeal(req, res, next) {
    logger.debug("Called signup meal");
    const studentHomeId = req.params.homeId;
    const mealId = Number(req.params.mealId);
    const userId = req.userId; // userId uit JWT token
    const signUpDate = new Date();

    databaseMeal.getStudentHomeMeals(studentHomeId, (err, result) => {
      if (err) {
        next(err);
      }
      if (result) {
        let meal = result.filter(function (e) {
          return e.ID === mealId;
        });
        if (!meal.length) {
          const error = {
            message: "getMealsDetails: No meal found for id: " + mealId,
            errCode: 404,
          };
          next(error);
        } else {
          database.getParticipants(mealId, (err, result) => {
            if (err) {
              next(err);
            }
            if (result) {
              if (meal[0].MaxParticipants >= result.length + 1) {
                database.insertParticipant(
                  studentHomeId,
                  mealId,
                  userId,
                  signUpDate,
                  (err, result) => {
                    if (err) {
                      next(err);
                    }
                    if (result) {
                      logger.debug("Participant has been sign up for meal");
                      res.status(200).json({
                        status: "success",
                        result: {
                          userId,
                          studentHomeId,
                          mealId,
                          signUpDate,
                        },
                      });
                    }
                  }
                );
              } else {
                const error = {
                  message:
                    "max participants of: " +
                    meal[0].MaxParticipants +
                    " already been reached",
                  errCode: 400,
                };
                next(error);
              }
            }
          });
        }
      }
    });
  },

  // UC-402 Afmelden voor maaltijd
  signOffMeal(req, res, next) {
    logger.debug("Called signOffMeal");
    const studentHomeId = req.params.homeId;
    const userId = req.userId; // userId uit JWT token
    const mealId = req.params.mealId;

    databaseMeal.getStudentHomeMeals(studentHomeId, (err, result) => {
      if (err) {
        next(err);
      }
      if (result) {
        let mealValidate = result.filter(function (e) {
          return e.ID == mealId;
        });
        if (!mealValidate.length) {
          const error = {
            message: "getMealsDetails: No meal found for id: " + mealId,
            errCode: 404,
          };
          next(error);
        } else {
          database.getParticipants(mealId, (err, result) => {
            if (err) {
              next(err);
            }
            if (result) {
              if (!result.length) {
                const error = {
                  message:
                    "getParticipants: No participants found for mealId: " +
                    mealId,
                  errCode: 404,
                };
                next(error);
              } else {
                let participantValidate = result.filter(function (e) {
                  return e.UserID == userId;
                });
                logger.warn(participantValidate);
                if (!participantValidate.length) {
                  const error = {
                    message:
                      "getParticipants: No participant found for id: " + userId,
                    errCode: 404,
                  };
                  next(error);
                } else {
                  database.deleteParticipant(
                    studentHomeId,
                    userId,
                    mealId,
                    (err, result) => {
                      if (err) {
                        next(err);
                      }
                      if (result) {
                        logger.debug("deleteParticipant was successfull");
                        res
                          .status(200)
                          .json({ status: "success", result: result.message });
                      }
                    }
                  );
                }
              }
            }
          });
        }
      }
    });
  },

  // UC-403 Lijst van deelnemers opvragen
  getParticipants(req, res, next) {
    logger.debug("Called getParticipants");
    const mealId = Number(req.params.mealId);

    database.getParticipants(mealId, (err, result) => {
      if (err) {
        next(err);
      }
      if (result) {
        if (!result.length) {
          const error = {
            message:
              "getParticipants: No participants found for mealId: " + mealId,
            errCode: 404,
          };
          next(error);
        } else {
          logger.debug("getParticipants was succesfull");
          res.status(200).json({ status: "success", result: result });
        }
      }
    });
  },

  // UC-404 Details van deelnemer opvragen
  getParticipant(req, res, next) {
    logger.debug("Called getParticipant");
    const mealId = Number(req.params.mealId);
    const participantId = req.params.participantId;

    database.getParticipants(mealId, (err, result) => {
      if (err) {
        next(err);
      }
      if (result) {
        if (!result.length) {
          const error = {
            message:
              "getParticipants: No participants found for mealId: " + mealId,
            errCode: 404,
          };
          next(error);
        } else {
          let participantValidate = result.filter(function (e) {
            return e.UserID == participantId;
          });
          logger.warn(participantValidate);
          if (!participantValidate.length) {
            const error = {
              message:
                "getParticipants: No participant found for id: " +
                participantId,
              errCode: 404,
            };
            next(error);
          } else {
            database.getParticipant(mealId, participantId, (err, result) => {
              if (err) {
                next(err);
              }
              if (result) {
                logger.debug("getParticipant was succesfull");
                res.status(200).json({ status: "success", result: result });
              }
            });
          }
        }
      }
    });
  },
};
