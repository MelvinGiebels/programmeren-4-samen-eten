const logger = require("tracer").colorConsole();
const pool = require("../dao/database");

module.exports = {
  // UC-401 Aanmelden voor maaltijd
  insertParticipant(studentHomeId, mealId, userId, signUpDate, callback) {
    let sqlQuery =
      "INSERT INTO participants (UserID, StudenthomeID, MealID, SignedUpOn) VALUES (?, ?, ?, ?)";
    logger.debug("insertParticipant", "sqlQuery =", sqlQuery);

    pool.getConnection((errCon, connection) => {
      if (errCon) {
        let error = {
          message:
            "insertParticipant failed getting connection! " + errCon.message,
          errCode: 400,
        };
        callback(error, undefined);
      }
      if (connection) {
        connection.query(
          sqlQuery,
          [userId, studentHomeId, mealId, signUpDate],
          (error, results, fields) => {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error) {
              const err = {
                message: "insertMeal failed calling query: " + error.message,
                errCode: 400,
              };
              callback(err, undefined);
            }
            if (results) {
              callback(undefined, results);
            }
          }
        );
      }
    });
  },

  // UC-402 Afmelden voor maaltijd
  deleteParticipant(studentHomeId, userId, mealId, callback) {
    let sqlQuery =
      "DELETE FROM participants WHERE UserID = ? AND StudenthomeID = ? AND MealID = ?";
    logger.debug("deleteParticipant", "sqlQuery =", sqlQuery);

    pool.getConnection((errCon, connection) => {
      if (errCon) {
        let error = {
          message: "deleteParticipant failed getting connection!",
          errCode: 400,
        };
        callback(error, undefined);
      }
      if (connection) {
        connection.query(
          sqlQuery,
          [userId, studentHomeId, mealId],
          (error, results, fields) => {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error) {
              const err = {
                message: "deleteParticipant failed calling query",
                errCode: 400,
              };
              callback(err, undefined);
            }
            if (results) {
              results.message =
                "Participant with ID: " +
                userId +
                " was deleted from meal with ID: " +
                mealId;
              callback(undefined, results);
            }
          }
        );
      }
    });
  },

  // UC-403 Lijst van deelnemers opvragen
  getParticipants(mealId, callback) {
    let sqlQuery = "SELECT * FROM participants WHERE MealID = ?";
    logger.debug("getParticipants", "sqlQuery =", sqlQuery);

    pool.getConnection((errCon, connection) => {
      if (errCon) {
        let error = {
          message: "getParticipants failed getting connection!",
          errCode: 400,
        };
        callback(error, undefined);
      }
      if (connection) {
        connection.query(sqlQuery, [mealId], (error, results, fields) => {
          // When done with the connection, release it.
          connection.release();

          // Handle error after the release.
          if (error) {
            const err = {
              message: "getParticipants failed calling query: " + error.message,
              errCode: 400,
            };
            callback(err, undefined);
          }
          if (results) {
            callback(undefined, results);
          }
        });
      }
    });
  },

  // UC-404 Details van deelnemer opvragen
  getParticipant(mealId, participantId, callback) {
    let sqlQuery = "SELECT * FROM participants WHERE MealID = ? AND UserID = ?";
    logger.debug("getParticipant", "sqlQuery =", sqlQuery);

    pool.getConnection((errCon, connection) => {
      if (errCon) {
        let error = {
          message: "getParticipant failed getting connection!",
          errCode: 400,
        };
        callback(error, undefined);
      }
      if (connection) {
        connection.query(
          sqlQuery,
          [mealId, participantId],
          (error, results, fields) => {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error) {
              const err = {
                message:
                  "getParticipant failed calling query: " + error.message,
                errCode: 400,
              };
              callback(err, undefined);
            }
            if (results) {
              callback(undefined, results);
            }
          }
        );
      }
    });
  },
};
