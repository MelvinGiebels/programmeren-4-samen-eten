const logger = require("tracer").colorConsole();
const pool = require("../dao/database");

module.exports = {
  insertStudentHome(studentHome, userId, callback) {
    let { name, address, houseNr, postalCode, phoneNr, city } = studentHome;

    let sqlQuery =
      "INSERT INTO studenthome (Name, Address, House_Nr, Postal_Code, Telephone, City, UserID) VALUES (?, ?, ?, ?, ?, ?, ?)";
    logger.debug("insertStudentHome", "sqlQuery =", sqlQuery);

    pool.getConnection((errCon, connection) => {
      if (errCon) {
        let error = {
          message:
            "insertStudentHome failed getting connection! " + errCon.message,
          errCode: 400,
        };
        callback(error, undefined);
      }
      if (connection) {
        connection.query(
          sqlQuery,
          [name, address, houseNr, postalCode, phoneNr, city, userId],
          (error, results, fields) => {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error) {
              const err = {
                message:
                  "insertStudentHome failed calling query: " + error.message,
                errCode: 400,
              };
              callback(err, undefined);
            }
            if (results) {
              callback(undefined, results);
            }
          }
        );
      }
    });
  },

  selectStudentHome(name, city, callback) {
    // Variable based on query parameters
    let queryPart = "";
    let errorMessage;

    if (name && city) {
      queryPart = "WHERE Name = '" + name + "' AND City = '" + city + "'";
      errorMessage =
        "No studentHome was found on name: " + name + " and city: " + city;
    } else if (name) {
      queryPart = "WHERE Name = '" + name + "'";
      errorMessage = "No studentHome was found on name: " + name;
    } else if (city) {
      queryPart = "WHERE City = '" + city + "'";
      errorMessage = "No studentHome was found on city: " + city;
    }

    let sqlQuery = "SELECT * FROM studenthome " + queryPart;
    logger.debug("selectStudentHome", "sqlQuery =", sqlQuery);

    pool.getConnection((errCon, connection) => {
      if (errCon) {
        let error = {
          message: "selectStudentHome failed getting connection!",
          errCode: 400,
        };
        callback(error, undefined);
      }
      if (connection) {
        connection.query(sqlQuery, (error, results, fields) => {
          // When done with the connection, release it.
          connection.release();

          // Handle error after the release.
          if (error) {
            const err = {
              message: "selectStudentHome failed calling query",
              errCode: 400,
            };
            callback(err, undefined);
          }
          if (results) {
            if (!results.length && errorMessage) {
              const err = {
                message: errorMessage,
                errCode: 404,
              };
              callback(err, undefined);
            } else {
              callback(undefined, results);
            }
          }
        });
      }
    });
  },

  getStudentHomeById(id, callback) {
    let sqlQuery = "SELECT * FROM studenthome WHERE ID = " + id;
    logger.debug("getStudentHomeById", "sqlQuery =", sqlQuery);

    pool.getConnection((errCon, connection) => {
      if (errCon) {
        let error = {
          message: "getStudentHomeById failed getting connection!",
          errCode: 400,
        };
        callback(error, undefined);
      }
      if (connection) {
        connection.query(sqlQuery, (error, results, fields) => {
          // When done with the connection, release it.
          connection.release();

          // Handle error after the release.
          if (error) {
            const err = {
              message: "getStudentHomeById failed calling query",
              errCode: 400,
            };
            callback(err, undefined);
          }
          if (results) {
            if (!results.length) {
              const err = {
                message: "No studentHome found for id: " + id,
                errCode: 404,
              };
              callback(err, undefined);
            } else {
              callback(undefined, results);
            }
          }
        });
      }
    });
  },

  updateStudentHome(homeId, studentHome, userId, callback) {
    let { name, address, houseNr, postalCode, phoneNr, city } = studentHome;

    let sqlQuery =
      "UPDATE studenthome SET Name = ?, Address = ?, House_Nr = ?, Postal_Code = ?, Telephone = ?, City = ? WHERE ID = ? AND UserID = ?";
    logger.debug("updateStudentHome", "sqlQuery =", sqlQuery);

    pool.getConnection((errCon, connection) => {
      if (errCon) {
        let error = {
          message: "updateStudentHome failed getting connection!",
          errCode: 400,
        };
        callback(error, undefined);
      }
      if (connection) {
        connection.query(
          sqlQuery,
          [name, address, houseNr, postalCode, phoneNr, city, homeId, userId],
          (error, results, fields) => {
            // When done with the connection, release it.
            connection.release();
            if (error) {
              const err = {
                message: "failed calling query",
                errCode: 400,
              };
              callback(err, undefined);
            }
            if (results) {
              callback(undefined, results);
            }
          }
        );
      }
    });
  },

  deleteStudentHome(homeId, userId, callback) {
    let sqlQuery =
      "DELETE FROM studenthome WHERE ID = " +
      homeId +
      " AND UserID = " +
      userId;
    logger.debug("deleteStudentHomeById", "sqlQuery =", sqlQuery);

    pool.getConnection((errCon, connection) => {
      if (errCon) {
        let error = {
          message: "deleteStudentHomeById failed getting connection!",
          errCode: 400,
        };
        callback(error, undefined);
      }
      if (connection) {
        connection.query(sqlQuery, (error, results, fields) => {
          // When done with the connection, release it.
          connection.release();
          if (error) {
            const err = {
              message: "failed calling query",
              errCode: 400,
            };
            callback(err, undefined);
          }
          if (results) {
            callback(undefined, results);
          }
        });
      }
    });
  },

  insertUserToHomeAdministrators(homeId, userId, callback) {
    let sqlQuery = "INSERT INTO home_administrators VALUES (?, ?)";
    logger.debug("getStudentHomeById", "sqlQuery =", sqlQuery);

    pool.getConnection((errCon, connection) => {
      if (errCon) {
        let error = {
          message: "getStudentHomeById failed getting connection!",
          errCode: 400,
        };
        callback(error, undefined);
      }
      if (connection) {
        connection.query(
          sqlQuery,
          [userId, homeId],
          (error, results, fields) => {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error) {
              logger.warn(error.message);
              const err = {
                message:
                  "insertUserToHomeAdministrators User with id: " +
                  userId +
                  " is already authorized",
                errCode: 400,
              };
              callback(err, undefined);
            }
            if (results) {
              callback(undefined, results);
            }
          }
        );
      }
    });
  },

  validateUser(homeId, userId, callback) {
    let sqlQuery =
      "SELECT * FROM home_administrators AS ha RIGHT JOIN studenthome AS sh ON ha.StudenthomeID = sh.ID WHERE ha.UserID = ? AND ha.StudenthomeID = ? OR sh.UserID = ? AND sh.ID = ?";
    logger.debug("validateUser", "sqlQuery =", sqlQuery);

    pool.getConnection((errCon, connection) => {
      if (errCon) {
        let error = {
          message: "validateUser failed getting connection!",
          errCode: 400,
        };
        callback(error, undefined);
      }
      if (connection) {
        connection.query(
          sqlQuery,
          [userId, homeId, userId, homeId],
          (error, result, fields) => {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error) {
              const err = {
                message: "HomeId doesn't exists " + error.message,
                errCode: 400,
              };
              callback(err, undefined);
            }
            if (result) {
              if (!result.length) {
                const err = {
                  message: "user with id: " + userId + " is not authorized",
                  errCode: 401,
                };
                callback(err, undefined);
              } else {
                callback(undefined, result);
              }
            }
          }
        );
      }
    });
  },
};
