const logger = require("tracer").colorConsole();
const pool = require("../dao/database");

module.exports = {
  insertMeal(studentHomeId, userId, meal, callback) {
    // Set current date as creationDate
    meal.creationDate = new Date();

    let {
      name,
      description,
      ingredients,
      allergyInfo,
      offerDate,
      price,
      creationDate,
    } = meal;

    let sqlQuery =
      "INSERT INTO meal (Name, Description, Ingredients, Allergies, CreatedOn, OfferedOn, Price, UserID, StudenthomeID, MaxParticipants) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    logger.debug("insertMeal", "sqlQuery =", sqlQuery);

    pool.getConnection((errCon, connection) => {
      if (errCon) {
        let error = {
          message: "insertMeal failed getting connection! " + errCon.message,
          errCode: 400,
        };
        callback(error, undefined);
      }
      if (connection) {
        connection.query(
          sqlQuery,
          [
            name,
            description,
            ingredients.join(", "),
            allergyInfo,
            creationDate,
            offerDate,
            price,
            userId,
            studentHomeId,
            0,
          ],
          (error, results, fields) => {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error) {
              logger.debug(error);
              const err = {
                message: "insertMeal failed calling query",
                errCode: 400,
              };
              callback(err, undefined);
            }
            if (results) {
              callback(undefined, results);
            }
          }
        );
      }
    });
  },

  updateMeal(studentHomeId, userId, mealId, meal, callback) {
    let { name, description, ingredients, allergyInfo, offerDate, price } =
      meal;

    let sqlQuery =
      "UPDATE meal SET Name = ?, Description = ?, Ingredients = ?, Allergies = ?, OfferedOn = ?, Price = ? WHERE ID = ? AND StudentHomeID = ? AND UserID = ?";
    logger.debug("updateMeal", "sqlQuery =", sqlQuery);

    pool.getConnection((errCon, connection) => {
      if (errCon) {
        let error = {
          message: "updateMeal failed getting connection!",
          errCode: 400,
        };
        callback(error, undefined);
      }
      if (connection) {
        connection.query(
          sqlQuery,
          [
            name,
            description,
            ingredients.join(", "),
            allergyInfo,
            offerDate,
            price,
            mealId,
            studentHomeId,
            userId,
          ],
          (error, results, fields) => {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error) {
              const err = {
                message: "updateMeal failed calling query",
                errCode: 400,
              };
              callback(err, undefined);
            }
            if (results) {
              if (!results.affectedRows) {
                const err = {
                  message:
                    "updateMeal user with id: " +
                    userId +
                    " was not authorized",
                  errCode: 401,
                };
                callback(err, undefined);
              } else {
                callback(undefined, results);
              }
            }
          }
        );
      }
    });
  },

  getStudentHomeMeals(studentHomeId, callback) {
    let sqlQuery = "SELECT * FROM meal WHERE StudentHomeID = " + studentHomeId;
    logger.debug("getMealById", "sqlQuery =", sqlQuery);

    pool.getConnection((errCon, connection) => {
      if (errCon) {
        let error = {
          message: "getMealById failed getting connection!",
          errCode: 400,
        };
        callback(error, undefined);
      }
      if (connection) {
        connection.query(sqlQuery, (error, results, fields) => {
          // When done with the connection, release it.
          connection.release();

          // Handle error after the release.
          if (error) {
            const err = {
              message: "getMealById failed calling query",
              errCode: 400,
            };
            callback(err, undefined);
          }
          if (results) {
            if (!results.length) {
              const err = {
                message: "No meal found for StudentHomeID: " + studentHomeId,
                errCode: 404,
              };
              callback(err, undefined);
            } else {
              results.forEach((result) => {
                result.Ingredients = result.Ingredients.split(", ");
              });
              callback(undefined, results);
            }
          }
        });
      }
    });
  },

  deleteMeal(studentHomeId, userId, mealId, callback) {
    let sqlQuery =
      "DELETE FROM meal WHERE ID = " +
      mealId +
      " AND StudenthomeID = " +
      studentHomeId +
      " AND UserID = " +
      userId;
    logger.debug("deleteMeal", "sqlQuery =", sqlQuery);

    pool.getConnection((errCon, connection) => {
      if (errCon) {
        let error = {
          message: "deleteMeal failed getting connection!",
          errCode: 400,
        };
        callback(error, undefined);
      }
      if (connection) {
        connection.query(sqlQuery, (error, results, fields) => {
          // When done with the connection, release it.
          connection.release();

          // Handle error after the release.
          if (error) {
            const err = {
              message: "deleteMeal failed calling query",
              errCode: 400,
            };
            callback(err, undefined);
          }
          if (results) {
            if (!results.affectedRows) {
              const err = {
                message:
                  "deleteMeal user with id: " + userId + " was not authorized",
                errCode: 401,
              };
              callback(err, undefined);
            } else {
              results.message = "Meal with id: " + mealId + " was deleted";
              callback(undefined, results);
            }
          }
        });
      }
    });
  },
};
