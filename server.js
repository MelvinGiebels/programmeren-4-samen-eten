const express = require("express");
const authenticationRoutes = require("./src/routes/authenticationRoute");
const studentHomeRoutes = require("./src/routes/studentHomeRoute");
const mealRoutes = require("./src/routes/mealsRoute");
const participateRoute =  require("./src/routes/participateRoute");

const app = express();
const port = process.env.PORT || 3000;

app.use(express.json()) // for parsing application/json

// middleware that is specific to this router
app.use(function timeLog(req, res, next) {
  console.log("Time: ", Date.now());
  next();
});

let logger = require("tracer").colorConsole();

// Authenticatie | UC-101 t/m UC-103
app.use("/api", authenticationRoutes);

// Beheer Studentenhuis | UC-201 t/m UC-206
app.use("/api/studenthome", studentHomeRoutes);

// Beheer Maaltijden | UC-301 t/m UC-305
app.use("/api/studenthome", mealRoutes);

// Beheer deelname | UC-401 t/m UC-404
app.use("/api", participateRoute);

// Generic handler
app.all(
  "*",
  (req, res, next) => {
    logger.debug("Generic logging handler called");
    next();
  },
  (req, res, next) => {
    const reqMethod = req.method;
    const reqUrl = req.url;
    logger.debug(reqMethod + " request at " + reqUrl);
    next();
  }
);

// Catch all endpoint
app.all("*", (req, res, next) => {
  logger.debug("Catch-all endpoint aangeroepen");
  next({ message: "Endpoint '" + req.url + "' does not exist", errCode: 401 });
});

// Error handler
app.use((error, req, res, next) => {
  logger.error("Errorhandler called! ", error);
  res.status(error.errCode).json({
    error: "Some error occurred",
    message: error.message,
  });
});

app.listen(port, () => {
  logger.log(`Example app listening at http://localhost:${port}`);
});


module.exports = app;