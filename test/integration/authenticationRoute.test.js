process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome_testdb";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";


const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../server");
const pool = require("../../src/dao/database");
const logger = require('../../src/config/config').logger
const assert = require("assert");
const bcrypt = require("bcrypt");

chai.should();
chai.use(chaiHttp);

logger.info(`Running tests using database '${process.env.DB_DATABASE}'`);

const CLEAR_DB = "DELETE IGNORE FROM `user`";


describe("Authentication", () => {
  before((done) => {
    logger.info("User tabel clear");
    pool.query(CLEAR_DB, (err, rows, fields) => {
      if (err) {
        logger.error(`beforeEach CLEAR error: ${err}`);
        done(err);
      } else {
        done();
      }
    });
  });

  // Add user to test TC-101-4 for duplicates 
  before((done) => {
    bcrypt.hash("secret", 10).then((hash) => {
      const ADD_USER =
  "INSERT INTO `user` (`ID`, `First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password`) VALUES('1', 'Jan', 'Smit', 'jsmit@server.nl','222222', ?),('2', 'Mark', 'Gerrits', 'mark@gerrits.nl', '333333', ?)";
      pool.query(ADD_USER, [hash, hash], (err, rows, fields) => {
        logger.info("Add user");
        if (err) {
          logger.error(`beforeEach CLEAR error: ${err}`);
          done(err);
        } else {
          done();
        }
      });
    });
    
  });

  // After successful register we have a valid token. We export this token
  // for usage in other testcases that require login.
  // let validToken

  describe("UC101 Registation", () => {
    it("TC-101-1 should throw an error when no firstname is provided", (done) => {
      chai
        .request(server)
        .post("/api/register")
        .send({
          // firstname is missing
          lastname: "LastName",
          email: "test@test.nl",
          studentnr: 123456789,
          password: "secret",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.body.message.should.be
            .a("string")
            .that.contains("firstname is invalid or missing.");

          done();
        });
    });
    it("TC-101-2 should throw an error when an invalid email is provided", (done) => {
      chai
        .request(server)
        .post("/api/register")
        .send({
          firstname: "firstName",
          lastname: "LastName",
          email: "test@@test.nl", // Email is invalid
          studentnr: 123456789,
          password: "secret",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.body.message.should.be
            .a("string")
            .that.contains("email was invalid format.");

          done();
        });
    });
    it("TC-101-3 should throw an error when an invalid password is provided", (done) => {
      chai
        .request(server)
        .post("/api/register")
        .send({
          firstname: "firstName",
          lastname: "LastName",
          email: "test@test.nl",
          studentnr: 123456789,
          password: 123, // Password is invalid
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.body.message.should.be
            .a("string")
            .that.contains("password is invalid or missing.");

          done();
        });
    });
    it("TC-101-4 should throw an error when an existing user is provided", (done) => {
      chai
        .request(server)
        .post("/api/register")
        .send({
          firstname: "Jan",
          lastname: "Smit",
          email: "jsmit@server.nl",
          studentnr: 222222,
          password: "secret",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.body.message.should.be
            .a("string")
            .that.contains("This email has already been taken.");

          done();
        });
    });
    it("TC-101-5 should return a token when providing valid information", (done) => {
      chai
        .request(server)
        .post("/api/register")
        .send({
          firstname: "FirstName",
          lastname: "LastName",
          email: "test@test.nl",
          studentnr: 1234567,
          password: "secret",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.body.should.be.a("object");

          const response = res.body;
          response.should.have.property("token").which.is.a("string");
          response.should.have.property("username").which.is.a("string");
          done();
        });
    });
  });
  describe("UC102 Login", () => {
    it("TC-101-1 should throw an error when no email is provided", (done) => {
      chai
        .request(server)
        .post("/api/login")
        .send({
          // email is missing
          password: "secret",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.body.message.should.be
            .a("string")
            .that.contains("email is invalid or missing.");

          done();
        });
    });
    it("TC-101-2 should throw an error when invalid email is provided", (done) => {
      chai
        .request(server)
        .post("/api/login")
        .send({
          email: "test@@test.nl",
          password: "secret",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.body.message.should.be
            .a("string")
            .that.contains("email was invalid format.");

          done();
        });
    });
    it("TC-101-3 should throw an error when invalid password is provided", (done) => {
      chai
        .request(server)
        .post("/api/login")
        .send({
          email: "test@test.nl",
          password: 123,
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.body.message.should.be
            .a("string")
            .that.contains("password is invalid or missing.");

          done();
        });
    });
    it("TC-101-4 should throw an error when user not exist in DB", (done) => {
      chai
        .request(server)
        .post("/api/login")
        .send({
          email: "email@email.com",
          password: "geheim",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(400);
          res.body.message.should.be
            .a("string")
            .that.contains("No User found for email: email@email.com");

          done();
        });
    });
    it("TC-102-5 should return a token when providing valid information", (done) => {
      chai
        .request(server)
        .post("/api/login")
        .send({
          email: "test@test.nl",
          password: "secret",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.body.should.be.a("object");
          const response = res.body;
          response.should.have.property("token").which.is.a("string");
          response.should.have.property('username').which.is.a('string')
          done();
        });
    });
  });
});
