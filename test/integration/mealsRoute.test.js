process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome_testdb";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../server");
const pool = require("../../src/dao/database");
const logger = require("../../src/config/config").logger;
const jwt = require("jsonwebtoken");
const assert = require("assert");

chai.should();
chai.use(chaiHttp);

logger.info(`Running tests using database '${process.env.DB_DATABASE}'`);

const CLEAR_DB = "DELETE IGNORE FROM `meal`";
const ADD_HOME =
  "INSERT INTO `studenthome` (`ID`, `Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES ('1', 'Princenhage', 'Princenhage', 11, 1,'4706RX','061234567891','Breda'),('2', 'Haagdijk 23', 'Haagdijk', 4, 1, '4706RX','061234567891','Breda')";
const ADD_MEAL =
  "INSERT INTO `meal` (`ID`, `Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `UserID`, `StudenthomeID`, `MaxParticipants`) VALUES('1', 'Zuurkool met worst', 'Zuurkool a la Montizaan, specialiteit van het huis.', 'Zuurkool, worst, spekjes', 'Lactose, gluten','2020-01-01 10:10','2020-01-02 10:10', 5.50, 1, 1, 10)";

describe("Meals", function () {
  before((done) => {
    logger.info("Meal tabel clear");
    pool.query(CLEAR_DB, (err, rows, fields) => {
      if (err) {
        logger.error(`beforeEach CLEAR error: ${err}`);
        done(err);
      } else {
        done();
      }
    });
  });

  // Add studentHome to test
  before((done) => {
    logger.info("studentHome added");
    pool.query(ADD_HOME, (err, rows, fields) => {
      logger.info("Add studentHome");
      if (err) {
        logger.error(`beforeEach CLEAR error: ${err}`);
        done(err);
      } else {
        done();
      }
    });
  });

  // Add meal to test
  before((done) => {
    logger.info("Meal added");
    pool.query(ADD_MEAL, (err, rows, fields) => {
      logger.info("Add meal");
      if (err) {
        logger.error(`beforeEach CLEAR error: ${err}`);
        done(err);
      } else {
        done();
      }
    });
  });

  ///// UC-301 Maaltijd aanmaken /////
  describe("create", function () {
    it("TC-301-1 | Required field is missing should return HTTP code 400 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .post("/api/studenthome/1/meal")
          .set("authorization", "Bearer " + token)
          .send({
            // name is missing
            description: "Soup with chicken",
            ingredients: ["Chicken", "Soup"],
            allergyInfo: "No allergies risk",
            offerDate: "2021-08-13T09:30:00",
            price: "3.20",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(400);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("message", "error");

            let { message, error } = res.body;
            message.should.be
              .a("string")
              .that.contains("name is invalid or missing");
            error.should.be.a("string");

            done();
          });
      });
    });
    it("TC-301-2 | should throw an 401 error when not logged in", (done) => {
      chai
        .request(server)
        .post("/api/studenthome/1/meal")
        .send({
          name: "Chicken soup",
          description: "Soup with chicken",
          ingredients: ["Chicken", "Soup"],
          allergyInfo: "No allergies risk",
          offerDate: "2021-08-13T09:30:00",
          price: "3.20",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(401);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("message", "datetime");

          res.body.message.should.be
            .a("string")
            .that.contains("Authorization header missing!");

          done();
        });
    });
    it("TC-301-3 | Meal was successfully added should return HTTP code 200 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .post("/api/studenthome/1/meal")
          .set("authorization", "Bearer " + token)
          .send({
            name: "Chicken soup",
            description: "Soup with chicken",
            ingredients: ["Chicken", "Soup"],
            allergyInfo: "No allergies risk",
            offerDate: "2021-08-13T09:30:00",
            price: "3.20",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(200);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("status", "result");

            let { status, result } = res.body;

            status.should.be.a("string").that.contains("success");
            result.should.be
              .an("object")
              .that.has.all.keys(
                "id",
                "name",
                "description",
                "ingredients",
                "allergyInfo",
                "offerDate",
                "price",
                "creationDate"
              );

            done();
          });
      });
    });
  });

  /// UC-302 Maaltijd wijzigen /////
  describe("put", function () {
    it("TC-302-1 | Required field is missing should return HTTP code 400 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .put("/api/studenthome/1/meal/1")
          .set("authorization", "Bearer " + token)
          .send({
            // name is missing
            description: "Soup with tomato",
            ingredients: ["Tomato", "Soup"],
            allergyInfo: "No allergies risk",
            offerDate: "2021-08-13T09:30:00",
            price: "5.25",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(400);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("message", "error");

            let { message, error } = res.body;
            message.should.be
              .a("string")
              .that.contains("name is invalid or missing");
            error.should.be.a("string");

            done();
          });
      });
    });
    it("TC-302-2 | should throw an 401 error when not logged in", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/1/meal/1")
        .send({
          name: "Tomato soup",
          description: "Soup with tomato",
          ingredients: ["Tomato", "Soup"],
          allergyInfo: "No allergies risk",
          offerDate: "2021-08-13T09:30:00",
          price: "5.25",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(401);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("message", "datetime");

          res.body.message.should.be
            .a("string")
            .that.contains("Authorization header missing!");

          done();
        });
    });
    it("TC-302-3 | should throw an 401 error if actor is not owner of meal", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .put("/api/studenthome/1/meal/1")
          .set(
            "authorization",
            "Bearer " + jwt.sign({ id: 2 }, "secret", { expiresIn: "2h" })
          )
          .send({
            name: "Tomato soup",
            description: "Soup with tomato",
            ingredients: ["Tomato", "Soup"],
            allergyInfo: "No allergies risk",
            offerDate: "2021-08-13T09:30:00",
            price: "5.25",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(401);
            res.should.be.an("object");
            res.body.should.be
              .an("object")
              .that.has.all.keys("error", "message");

            res.body.message.should.be
              .a("string")
              .that.contains("user with id: 2 was not authorized");

            done();
          });
      });
    });

    it("TC-302-4 | Meal doesn't exist should return HTTP code 404 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .put("/api/studenthome/1/meal/-1")
          .set("authorization", "Bearer " + token)
          .send({
            name: "Tomato soup",
            description: "Soup with tomato",
            ingredients: ["Tomato", "Soup"],
            allergyInfo: "No allergies risk",
            offerDate: "2021-08-13T09:30:00",
            price: "5.25",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(404);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("message", "error");

            let { message, error } = res.body;
            message.should.be
              .a("string")
              .that.contains("No meal found for id:");
            error.should.be.a("string");

            done();
          });
      });
    });

    it("TC-302-5 | Meal was successfully updated should return HTTP code 200 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .put("/api/studenthome/1/meal/1")
          .set("authorization", "Bearer " + token)
          .send({
            id: null,
            name: "Chicken soup",
            description: "Soup with chicken",
            offerDate: "1995-12-17T03:24:00",
            price: "1.20",
            allergyInfo: "No allergies risk",
            ingredients: ["Chicken", "Soup"],
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(200);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("status", "result");

            let { status, result } = res.body;
            status.should.be.a("string").that.contains("success");
            result.should.be.a("object");

            done();
          });
      });
    });
  });

  ///// UC-303 Lijst van maaltijden opvragen /////
  describe("get", function () {
    it("TC-303-1 | Show list of meals return HTTP code 200 and JSON object with all meals", (done) => {
      chai
        .request(server)
        .get("/api/studenthome/1/meal")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.should.be.an("object").that.has.all.keys("status", "result");

          let { status, result } = res.body;
          status.should.be.a("string").that.contains("success");
          result.should.be.a("array");

          done();
        });
    });
    it("TC-304-1 | MealId does not exist return HTTP code 404 and JSON object with errorMsg", (done) => {
      chai
        .request(server)
        .get("/api/studenthome/1/meal/-1")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(404);
          res.should.be.an("object");

          res.body.should.be.an("object").that.has.all.keys("message", "error");

          let { message, error } = res.body;
          message.should.be.a("string").that.contains("No meal found for id:");
          error.should.be.a("string");

          done();
        });
    });
    it("TC-304-2 | MealId does exist return HTTP code 200 and JSON object with information of the meal", (done) => {
      chai
        .request(server)
        .get("/api/studenthome/1/meal/1")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.should.be.an("object").that.has.all.keys("status", "result");

          let { status, result } = res.body;
          status.should.be.a("string").that.contains("success");
          result.should.be.a("array");

          done();
        });
    });
  });
  ///// UC-305 Maaltijd verwijderen /////
  describe("delete", function () {
    it("TC-305-1 | StudentHome doesn't exist should return HTTP code 404 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .delete("/api/studenthome/-1/meal/1")
          .set("authorization", "Bearer " + token)
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(404);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("message", "error");

            let { message, error } = res.body;
            message.should.be
              .a("string")
              .that.contains("No meal found for StudentHomeID: -1");
            error.should.be.a("string");

            done();
          });
      });
    });

    it("TC-305-2 | should throw an 401 error when not logged in", (done) => {
      chai
        .request(server)
        .delete("/api/studenthome/1/meal/1")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(401);
          res.should.be.an("object");
          res.body.should.be
            .an("object")
            .that.has.all.keys("message", "datetime");

          res.body.message.should.be
            .a("string")
            .that.contains("Authorization header missing!");

          done();
        });
    });
    it("TC-305-3 | should throw an 401 error if actor is not owner of meal", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .delete("/api/studenthome/1/meal/1")
          .set(
            "authorization",
            "Bearer " + jwt.sign({ id: 2 }, "secret", { expiresIn: "2h" })
          )
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(401);
            res.should.be.an("object");
            res.body.should.be
              .an("object")
              .that.has.all.keys("error", "message");

            res.body.message.should.be
              .a("string")
              .that.contains("user with id: 2 was not authorized");

            done();
          });
      });
    });

    it("TC-305-4 | Meal doesn't exist should return HTTP code 404 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .delete("/api/studenthome/1/meal/-1")
          .set("authorization", "Bearer " + token)
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(404);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("message", "error");

            let { message, error } = res.body;
            message.should.be.a("string").that.contains("No meal found for id");
            error.should.be.a("string");

            done();
          });
      });
    });

    it("TC-305-5 | Meal was successfully deleted should return HTTP code 200 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .delete("/api/studenthome/1/meal/1")
          .set("authorization", "Bearer " + token)
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(200);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("status", "result");

            let { status, result } = res.body;
            status.should.be.a("string").that.contains("success");
            result.should.be
              .a("string")
              .that.contains("Meal with id: 1 was deleted");

            done();
          });
      });
    });
  });
});
