process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome_testdb";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../server");
const pool = require("../../src/dao/database");
const logger = require("../../src/config/config").logger;
const jwt = require("jsonwebtoken");
const assert = require("assert");

chai.should();
chai.use(chaiHttp);

logger.info(`Running tests using database '${process.env.DB_DATABASE}'`);

const CLEAR_DB = "DELETE IGNORE FROM `studenthome`";
const ADD_HOMES =
  "INSERT INTO `studenthome` (`ID`, `Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES ('1', 'Princenhage', 'Princenhage', 11, 1,'4706RX','061234567891','Breda'),('2', 'Haagdijk 23', 'Haagdijk', 4, 1, '4706RX','061234567891','Breda')";
const ADD_HOME_ADMIN =
"INSERT INTO `home_administrators` (`UserId`, `StudenthomeID`) VALUES (1, 1)";


describe("StudentHome", function () {
  before((done) => {
    logger.info("studenthome tabel clear");
    pool.query(CLEAR_DB, (err, rows, fields) => {
      if (err) {
        logger.error(`before CLEAR error: ${err}`);
        done(err);
      } else {
        done();
      }
    });
  });

  // Add studentHome to test TC-201-4 duplicate address
  before((done) => {
    logger.info("studenthome added");
    pool.query(ADD_HOMES, (err, rows, fields) => {
      if (err) {
        logger.error(`before CLEAR error: ${err}`);
        done(err);
      } else {
        done();
      }
    });
  });

  before((done) => {
    logger.info("studenthome admin added");
    pool.query(ADD_HOME_ADMIN, (err, rows, fields) => {
      if (err) {
        logger.error(`before CLEAR error: ${err}`);
        done(err);
      } else {
        done();
      }
    });
  });

  ///// UC-201 Maak studentenhuis /////
  describe("create", function () {
    jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
      it("TC-201-1 | Required field is missing should return HTTP code 400 and JSON object", (done) => {
        chai
          .request(server)
          .post("/api/studenthome")
          .set("authorization", "Bearer " + token)
          .send({
            // name is missing
            address: "Teststraat",
            houseNr: 10,
            postalCode: "1234AB",
            phoneNr: "0612345678",
            city: "Breda",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(400);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("message", "error");

            let { message, error } = res.body;
            message.should.be
              .a("string")
              .that.contains("is invalid or missing");
            error.should.be.a("string");

            done();
          });
      });
    });

    it("TC-201-2 | Invalid postalcode should return HTTP code 400 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .post("/api/studenthome")
          .set("authorization", "Bearer " + token)
          .send({
            name: "Test StudentHuis",
            address: "Teststraat",
            houseNr: 10,
            postalCode: "12", // Invalid postalCode
            phoneNr: "0612345678",
            city: "Breda",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(400);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("message", "error");

            let { message, error } = res.body;
            message.should.be
              .a("string")
              .that.contains("postalCode was invalid");
            error.should.be.a("string");

            done();
          });
      });
    });

    it("TC-201-3 | Invalid phoneNr should return HTTP code 400 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .post("/api/studenthome")
          .set("authorization", "Bearer " + token)
          .send({
            name: "Test StudentHuis",
            address: "Teststraat",
            houseNr: 10,
            postalCode: "1234AB",
            phoneNr: "asd", // Invalid phoneNr
            city: "Breda",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(400);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("message", "error");

            let { message, error } = res.body;
            message.should.be.a("string").that.contains("phoneNr was invalid");
            error.should.be.a("string");

            done();
          });
      });
    });

    it("TC-201-4 | StudentHome already exist on given address should return HTTP code 400 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .post("/api/studenthome")
          .set("authorization", "Bearer " + token)
          .send({
            name: "Test StudentHuis",
            address: "Princenhage",
            houseNr: 11,
            postalCode: "4706RX",
            phoneNr: "6123456789",
            city: "Breda",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(400);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("message", "error");

            let { message, error } = res.body;
            message.should.be
              .a("string")
              .that.contains(
                "Duplicate entry '4706RX-11' for key 'UniqueAdress'"
              );
            error.should.be.a("string");

            done();
          });
      });
    });

    it("TC-201-5 | should throw an 401 error when not logged in", (done) => {
      chai
        .request(server)
        .post("/api/studenthome")
        .send({
          name: "Test StudentHuis",
          address: "Teststraat",
          houseNr: 10,
          postalCode: "1234AB",
          phoneNr: "0612345678",
          city: "Breda",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(401);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("message", "datetime");

          res.body.message.should.be
            .a("string")
            .that.contains("Authorization header missing!");

          done();
        });
    });
    it("TC-201-6 | StudentHome was successfully added should return HTTP code 200 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .post("/api/studenthome")
          .set("authorization", "Bearer " + token)
          .send({
            name: "Test StudentHuis",
            address: "Teststraat",
            houseNr: 10,
            postalCode: "1234AB",
            phoneNr: "0612345678",
            city: "Breda",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(200);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("status", "result");

            let { status, result } = res.body;
            status.should.be.a("string").that.contains("success");

            result.should.be.an("Object").that.contains({
              name: "Test StudentHuis",
              address: "Teststraat",
              houseNr: 10,
              postalCode: "1234AB",
              phoneNr: "0612345678",
              city: "Breda",
            });
            done();
          });
      });
    });
    it("TC-206-1 | Required field is missing should return HTTP code 400 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .post("/api/studenthome/1/user")
          .set("authorization", "Bearer " + token)
          .send({
            // id is missing
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(400);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("error", "message");

            let { error, message } = res.body;
            error.should.be.a("string");
            message.should.be
              .a("string")
              .that.contains("Missing id in request body");

            done();
          });
      });
    });
    it("TC-205-2 | should throw an 401 error when not logged in", (done) => {
      chai
        .request(server)
        .post("/api/studenthome/1/user")
        .send({
          id: 2
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(401);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("message", "datetime");

          res.body.message.should.be
            .a("string")
            .that.contains("Authorization header missing!");

          done();
        });
    });
    it("TC-206-3 | Not an admin of studentHome should return HTTP code 401 and JSON object", (done) => {
      jwt.sign({ id: 2 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .post("/api/studenthome/1/user")
          .set("authorization", "Bearer " + token)
          .send({
            id: 3
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(401);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("error", "message");

            let { error, message } = res.body;
            error.should.be.a("string");
            message.should.be
              .a("string")
              .that.contains("user with id: 2 is not authorized");

            done();
          });
      });
    });
    it("TC-206-4 | User already authorized turn HTTP code 400 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .post("/api/studenthome/1/user")
          .set("authorization", "Bearer " + token)
          .send({
            id: 1
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(400);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("error", "message");

            let { error, message } = res.body;
            error.should.be.a("string");
            message.should.be
              .a("string")
              .that.contains("User with id: 1 is already authorized");

            done();
          });
      });
    });
    it("TC-206-5 | Admin added other user succesfully should return HTTP code 200 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .post("/api/studenthome/1/user")
          .set("authorization", "Bearer " + token)
          .send({
            id: 2
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(200);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("status", "result");

            let { status, result } = res.body;
            status.should.be.a("string").that.contains("succes");
            result.should.be
              .a("string")
              .that.contains("User with id: 2 has been added");

            done();
          });
      });
    });

  });

  ///// UC-202 Overzicht van studentenhuizen /////
  describe("get", function () {
    before((done) => {
      logger.info("studenthome tabel clear");
      pool.query(CLEAR_DB, (err, rows, fields) => {
        if (err) {
          logger.error(`beforeEach CLEAR error: ${err}`);
          done(err);
        } else {
          done();
        }
      });
    });
    it("TC-202-1 | Show 0 studentHomes return HTTP code 200 and an empty JSON object", (done) => {
      chai
        .request(server)
        .get("/api/studenthome")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.should.be.an("object").that.has.all.keys("status", "result");

          let { status, result } = res.body;
          logger.debug(res.body);
          status.should.be.a("string").that.contains("success");
          result.should.be.a("array").that.has.lengthOf(0);

          done();
        });
    });

    it("TC-202-2 | Show 2 studentHomes return HTTP code 200 and JSON object with 2 studenthomes", (done) => {
      pool.query(ADD_HOMES, (error, resultAdd) => {
        if (error) {
          logger.error(`CLEAR error: ${err}`);
          done(err);
        } else if (resultAdd) {
          chai
            .request(server)
            .get("/api/studenthome?city=Breda")
            .end((err, res) => {
              assert.ifError(err);
              res.should.have.status(200);
              res.should.be.an("object");

              res.body.should.be
                .an("object")
                .that.has.all.keys("status", "result");

              let { status, result } = res.body;
              status.should.be.a("string").that.contains("success");
              result.should.be.an("array").that.has.lengthOf(2);

              done();
            });
        }
      });
    });
    it("TC-202-3 | Show studentHomes on not existing city return HTTP code 404 and JSON object with errorMsg", (done) => {
      chai
        .request(server)
        .get("/api/studenthome?city=asdasdasd")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(404);
          res.should.be.an("object");

          res.body.should.be.an("object").that.has.all.keys("message", "error");

          let { message, error } = res.body;
          message.should.be
            .a("string")
            .that.contains("No studentHome was found on city:");
          error.should.be.a("string");

          done();
        });
    });
    it("TC-202-4 | Show studentHomes on not existing name return HTTP code 404 and JSON object with errorMsg", (done) => {
      chai
        .request(server)
        .get("/api/studenthome?name=asdasdasd")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(404);
          res.should.be.an("object");

          res.body.should.be.an("object").that.has.all.keys("message", "error");

          let { message, error } = res.body;
          message.should.be
            .a("string")
            .that.contains("No studentHome was found on name:");
          error.should.be.a("string");

          done();
        });
    });
    it("TC-202-5 | Show studentHomes on existing city return HTTP code 200 and JSON object with the found studentHome", (done) => {
      chai
        .request(server)
        .get("/api/studenthome?city=Breda")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.should.be.an("object").that.has.all.keys("status", "result");

          let { status, result } = res.body;
          status.should.be.a("string").that.contains("success");
          result.should.be.a("array");

          // Check if every object contains the given city
          result.forEach((studentHome) => {
            studentHome.should.be.an("Object").that.contains({ City: "Breda" });
          });

          done();
        });
    });
    it("TC-202-6 | Show studentHomes on existing name return HTTP code 200 and JSON object with the found studentHome", (done) => {
      chai
        .request(server)
        .get("/api/studenthome?name=Princenhage")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.should.be.an("object").that.has.all.keys("status", "result");

          let { status, result } = res.body;
          status.should.be.a("string").that.contains("success");
          result.should.be.a("array");

          // Check if every object contains the given name
          result.forEach((studentHome) => {
            studentHome.should.be
              .an("Object")
              .that.contains({ Name: "Princenhage" });
          });

          done();
        });
    });
    it("TC-203-1 | StudentHomeId does not exist return HTTP code 404 and JSON object with errorMsg", (done) => {
      chai
        .request(server)
        .get("/api/studenthome/-1")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(404);
          res.should.be.an("object");

          res.body.should.be.an("object").that.has.all.keys("message", "error");

          let { message, error } = res.body;
          message.should.be
            .a("string")
            .that.contains("No studentHome found for id:");
          error.should.be.a("string");

          done();
        });
    });
    it("TC-203-2 | StudentHomeId does exist return HTTP code 200 and JSON object with information of studentHome", (done) => {
      chai
        .request(server)
        .get("/api/studenthome/1")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(200);
          res.should.be.an("object");

          res.body.should.be.an("object").that.has.all.keys("status", "result");

          let { status, result } = res.body;
          status.should.be.a("string").that.contains("success");
          result[0].should.be.an("Object").that.contains({
            ID: 1,
            Name: "Princenhage",
            Address: "Princenhage",
            House_Nr: 11,
            UserID: 1,
            Postal_Code: "4706RX",
            Telephone: "061234567891",
            City: "Breda",
          });

          done();
        });
    });
  });

  ///// UC-204 Studentenhuis wijzigen /////
  describe("put", function () {
    it("TC-204-1 | Required field is missing should return HTTP code 400 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .put("/api/studenthome/1")
          .set("authorization", "Bearer " + token)
          .send({
            // name is missing
            address: "Teststraat",
            houseNr: 10,
            postalCode: "1234AB",
            phoneNr: "0612345678",
            city: "Breda",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(400);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("message", "error");

            let { message, error } = res.body;
            message.should.be
              .a("string")
              .that.contains("is invalid or missing");
            error.should.be.a("string");

            done();
          });
      });
    });

    it("TC-204-2 | Invalid postalcode should return HTTP code 400 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .put("/api/studenthome/1")
          .set("authorization", "Bearer " + token)
          .send({
            name: "Test StudentHuis",
            address: "Teststraat",
            houseNr: 10,
            postalCode: "12", // Invalid postalCode
            phoneNr: "0612345678",
            city: "Breda",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(400);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("message", "error");

            let { message, error } = res.body;
            message.should.be
              .a("string")
              .that.contains("postalCode was invalid");
            error.should.be.a("string");

            done();
          });
      });
    });

    it("TC-204-3 | Invalid phoneNr should return HTTP code 400 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .put("/api/studenthome/1")
          .set("authorization", "Bearer " + token)
          .send({
            name: "Test StudentHuis",
            address: "Teststraat",
            houseNr: 10,
            postalCode: "1234AB",
            phoneNr: "asd", // Invalid phoneNr
            city: "Breda",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(400);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("message", "error");

            let { message, error } = res.body;
            message.should.be.a("string").that.contains("phoneNr was invalid");
            error.should.be.a("string");

            done();
          });
      });
    });

    it("TC-204-4 | StudentHome doesn't exist should return HTTP code 404 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .put("/api/studenthome/-1")
          .set("authorization", "Bearer " + token)
          .send({
            name: "Test StudentHuis",
            address: "Teststraat",
            houseNr: 10,
            postalCode: "1234AB",
            phoneNr: "0612345678",
            city: "Breda",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(404);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("message", "error");

            let { message, error } = res.body;
            message.should.be
              .a("string")
              .that.contains("No studentHome found for id");
            error.should.be.a("string");

            done();
          });
      });
    });

    it("TC-204-5 | should throw an 401 error when not logged in", (done) => {
      chai
        .request(server)
        .put("/api/studenthome/1")
        .send({
          name: "Test Update",
          address: "TestUpdateStraat",
          houseNr: 12,
          postalCode: "4321BA",
          phoneNr: "0612345678",
          city: "UpdateCity",
        })
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(401);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("message", "datetime");

          res.body.message.should.be
            .a("string")
            .that.contains("Authorization header missing!");

          done();
        });
    });
    it("TC-204-6 | StudentHome was successfully updated should return HTTP code 200 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .put("/api/studenthome/1")
          .set("authorization", "Bearer " + token)
          .send({
            name: "Test Update",
            address: "TestUpdateStraat",
            houseNr: 12,
            postalCode: "4321BA",
            phoneNr: "0612345678",
            city: "UpdateCity",
          })
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(200);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("status", "result");

            let { status, result } = res.body;
            status.should.be.a("string").that.contains("success");

            result.should.be.an("object").that.eql({
              id: 1,
              name: "Test Update",
              address: "TestUpdateStraat",
              houseNr: 12,
              postalCode: "4321BA",
              phoneNr: "0612345678",
              city: "UpdateCity",
            });

            done();
          });
      });
    });
  });

  ///// UC-205 Studentenhuis verwijderen /////
  describe("delete", function () {
    it("TC-205-1 | StudentHome doesn't exist should return HTTP code 404 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .delete("/api/studenthome/-1")
          .set("authorization", "Bearer " + token)
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(404);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("message", "error");

            let { message, error } = res.body;
            message.should.be
              .a("string")
              .that.contains("No studentHome found for id:");
            error.should.be.a("string");

            done();
          });
      });
    });

    it("TC-205-2 | should throw an 401 error when not logged in", (done) => {
      chai
        .request(server)
        .delete("/api/studenthome/1")
        .end((err, res) => {
          assert.ifError(err);
          res.should.have.status(401);
          res.should.be.an("object");

          res.body.should.be
            .an("object")
            .that.has.all.keys("message", "datetime");

          res.body.message.should.be
            .a("string")
            .that.contains("Authorization header missing!");

          done();
        });
    });
    it("TC-205-3 | should throw an 401 error if actor is not owner of studenthome", (done) => {
      jwt.sign({ id: 2 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .delete("/api/studenthome/1")
          .set(
            "authorization",
            "Bearer " + jwt.sign({ id: 2 }, "secret", { expiresIn: "2h" })
          )
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(401);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("message", "error");

            let { message, error } = res.body;
            message.should.be
              .a("string")
              .that.contains("user with id: 2 is not authorized");
            error.should.be.a("string");

            done();
          });
      });
    });

    it("TC-205-4 | StudentHome was successfully deleted should return HTTP code 200 and JSON object", (done) => {
      jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
        chai
          .request(server)
          .delete("/api/studenthome/2")
          .set("authorization", "Bearer " + token)
          .end((err, res) => {
            assert.ifError(err);
            res.should.have.status(200);
            res.should.be.an("object");

            res.body.should.be
              .an("object")
              .that.has.all.keys("status", "result");

            let { status, result } = res.body;
            status.should.be.a("string").that.contains("success");
            result.should.be.a("array").that.has.lengthOf(1);

            result[0].should.be.an("Object").that.contains({
              ID: 2,
            });

            done();
          });
      });
    });
    
  });
});
